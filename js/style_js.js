// logo loader (っ◔◡◔)っ ♥
$(document).ready(function () {
  setTimeout(function () {
    $('#loading').fadeOut(1000);
  }, 1000);
});

$(document).ready(function() {
  wow = new WOW({
    boxClass: "wow", // default
    animateClass: "animated", // default
    offset: 0, // default
    mobile: false, // default
    live: true // default
  });
  new WOW().init();
});



// customization for Owl-carousel (っ◔◡◔)っ ♥
$(document).ready(function () {
  $("#carousel").owlCarousel({
    autoplay: false,
    autoplayHoverPause: true,
    dots: true,
    rewind: true,
    items: 3,
    responsive: {
      320: { items: 1 },
      768: { items: 2 },
      1300: { items: 3 },
    },
  });
});

$(document).ready(function () {
  $("#caro").owlCarousel({
    autoplay: true,
    autoplayHoverPause: true,
    dots: true,
    rewind: true,
    item: 2,
    responsive: {
      320: { items: 1 },
      768: { items: 2 },
    },
  });
});

// header effects (っ◔◡◔)っ ♥
$(window).scroll(function () {
  var sc = $(window).scrollTop()
  if (sc > 50) {
    $("#header-sroll").addClass("small")
    $("#header-sroll").addClass("img_stl")
    $("#header-sroll").addClass("img_s_none")
    $("#header-sroll").addClass("img_f_none")
    $("#header-sroll").addClass("text_stl")
  } else {
    $("#header-sroll").removeClass("small")
    $("#header-sroll").removeClass("img_stl")
    $("#header-sroll").removeClass("img_s_none")
    $("#header-sroll").removeClass("img_f_none")
    $("#header-sroll").removeClass("text_stl")
  }

});


$(document).ready(function () {
  $(window).scroll(function () {
    if ($(this).scrollTop() > 100) {
      $('.scroll-top').fadeIn();
    } else {
      $('.scroll-top').fadeOut();
    }
  });

  $('.scroll-top').click(function () {
    $("html, body").animate({
      scrollTop: 0
    }, 1000);
    return false;
  });

});

// smooth scrolling js (っ◔◡◔)っ ♥
$(document).ready(function () {
  $('body').scrollspy({ target: ".navbar", offset: 100 });
  $("#myNavbar a").on('click', function (event) {
    if (this.hash !== "") {
      event.preventDefault();
      var hash = this.hash;
      $('html, body').animate({
        scrollTop: $(hash).offset().top
      }, 800, function () {
        window.location.hash = hash;
      });
    }
  });
});

//screenshots image filtering (っ◔◡◔)っ ♥
$(document).ready(function () {

  $(".filter_btn").click(function () {
    $(this).addClass('active_m').siblings().removeClass('active_m');
    var value = $(this).attr('data-filter');

    if (value == "all") {
      $('.filter').show('1000');
    }
    else {
      $(".filter").not('.' + value).hide('3000');
      $('.filter').filter('.' + value).show('3000');
    }
  });
});


// navigate scroll custom
$(document).ready(function () {
  $(document).on("scroll", onScroll);
  
  
  $('a[href^="#"]').on('click', function (e) {
      e.preventDefault();
      $(document).off("scroll");
      
      $('a').each(function () {
          $(this).removeClass('active');
      })
      $(this).addClass('active');
    
      var target = this.hash,
          menu = target;
      $target = $(target);
      $('html, body').stop().animate({
          'scrollTop': $target.offset().top+2
      }, 500, 'swing', function () {
          window.location.hash = target;
          $(document).on("scroll", onScroll);
      });
  });
});

function onScroll(event){
  var scrollPos = $(document).scrollTop();
  $('#menu-center a').each(function () {
      var currLink = $(this);
      var refElement = $(currLink.attr("href"));
      if (refElement.position().top <= scrollPos && refElement.position().top + refElement.height() > scrollPos) {
          $('#menu-center ul li a').removeClass("active");
          currLink.addClass("active");
      }
      else{
          currLink.removeClass("active");
      }
  });
}
